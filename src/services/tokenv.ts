import jwt from 'jsonwebtoken';

const SECRET_KEY = 'tu_secreto';  // Deberías cambiar esto y mantenerlo seguro

// Crear un token
function createToken(userId: string): string {
  return jwt.sign({ userId }, SECRET_KEY, { expiresIn: '1h' });  // El token expira en 1 hora
}

// Verificar un token
function verifyToken(token: string): any {
  try {
    const decoded = jwt.verify(token, SECRET_KEY);
    return decoded;
  } catch (error) {
    console.error('Error al verificar el token:', error);
    return null;
  }
}

// Almacenar el token en una cookie
function setTokenCookie(token: string) {
    const date = new Date();
    date.setTime(date.getTime() + (1 * 60 * 60 * 1000));  // 1 hora de expiración
    const expires = "expires=" + date.toUTCString();
    document.cookie = `jwtToken=${token}; ${expires}; path=/`;
  }
  
  // Obtener el token de la cookie
  function getTokenCookie(): string | null {
    const name = 'jwtToken=';
    const decodedCookie = decodeURIComponent(document.cookie);
    const cookieArray = decodedCookie.split(';');
    
    for (let i = 0; i < cookieArray.length; i++) {
      let cookie = cookieArray[i];
      while (cookie.charAt(0) === ' ') {
        cookie = cookie.substring(1);
      }
      if (cookie.indexOf(name) === 0) {
        return cookie.substring(name.length, cookie.length);
      }
    }
    return null;
  }
  