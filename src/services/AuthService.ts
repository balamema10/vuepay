import type ITpagos from "@/interface/ITpagos";

const API_URL = "http://localhost:5000/api";

export default {
  async getTpagos(userId: string): Promise<ITpagos[]> {
    try {
      const response = await fetch(`${API_URL}/tpagos/${userId}`, {
        method: "GET",
      });
      if (!response.ok) {
        throw new Error("No se pudieron recuperar los datos de los pagos");
      }
      return await response.json();
    } catch (error) {
      console.error("Error al obtener los pagos:", error);
      throw error;
    }
  },

  async registerUser(name: string, apellido: string, email: string, password: string): Promise<void> {
    try {
      const userData = { name, apellido, email, password };
      const response = await fetch(`${API_URL}/insertdata/`, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(userData),
      });
      if (!response.ok) {
        const errorData = await response.json();
        throw new Error(errorData.message || "No se pudo registrar el usuario");
      }
      console.log("Usuario registrado exitosamente");
    } catch (error) {
      console.error("Error al registrar el usuario:", error);
      throw error;
    }
  },

  async loginUser(email: string, password: string): Promise<{ token: string }> {
    try {
      const userData = { email, password };
      const response = await fetch(`${API_URL}/login/`, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(userData),
      });
      if (!response.ok) {
        const errorData = await response.json();
        throw new Error(errorData.error || "No se pudo acceder al usuario");
      }
      const responseData = await response.json();
      console.log("Respuesta del servidor:", responseData);
      return responseData;
    } catch (error) {
      console.error("Error al acceder al usuario:", error);
      throw error;
    }
  },

  async getUserById(userId: string): Promise<{ user: any }> {
    try {
      const response = await fetch(`${API_URL}/users/${userId}`, {
        method: "GET",
      });
      if (!response.ok) {
        const errorData = await response.json();
        throw new Error(errorData.error || "No se pudo obtener el usuario");
      }
      const responseData = await response.json();
      console.log("Datos del usuario:", responseData);
      return responseData;
    } catch (error) {
      console.error("Error al obtener el usuario por ID:", error);
      throw error;
    }
  }
};
