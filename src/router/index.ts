import { createRouter, createWebHistory } from 'vue-router';
import type { RouteRecordRaw } from 'vue-router';

import LoginView from '@/views/LoginView.vue';
import Tpagos from '@/views/TpagosView.vue';
import VistaUser from '@/views/VusuariosView.vue'
import salir from '@/views/salirView.vue'
import RegisterItems from '@/views/RegisterView.vue'
import GpagosView from '@/views/GpagosView.vue';

const BASE_URL = '/';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    name: 'Login',
    component: LoginView,
  },
  {
    path: '/RegisterItems',
    name: 'RegisterItems',
    component: RegisterItems,
  },
  {
    path: '/VistaUser/:id',
    name: 'VistaUser',
    component: VistaUser,
    meta: { requiresAuth: true },
  },
  {
    path: '/VistaUser/',
    name: 'VistaUser',
    component: VistaUser,
    meta: { requiresAuth: true },
  },
  {
    path: '/Tpagos/',
    name: 'Tpagos',
    component: Tpagos,
    meta: { requiresAuth: true },
  },
  {
    path: '/salir',
    name: 'salir',
    component: salir,
    meta: { requiresAuth: true },
  },
  {
    path: '/GpagosView',
    name: 'GpagosView',
    component: GpagosView,
    meta: { requiresAuth: true },
  }
];

const router = createRouter({
  history: createWebHistory(BASE_URL),
  routes,
});

export default router;
