const express = require('express');
const sql = require('mssql');
const cors = require('cors'); 

// Configuración de la conexión a la base de datos
const dbConfig = {
  server: process.env.DB_SERVER || 'localhost',
  port: parseInt(process.env.DB_PORT, 10) || 1433,
  database: process.env.DB_DATABASE || "vuepay",
  user: process.env.DB_USER || "sa",
  password: process.env.DB_PASSWORD || "1234",
  options: {
    encrypt: true,
    trustServerCertificate: true
  }
};

// Crear una instancia de Express
const app = express();

// Middleware para permitir el análisis de JSON en las solicitudes
app.use(express.json());

// Habilitar CORS para todas las solicitudes
app.use(cors());

// Endpoint para obtener datos de la base de datos
app.get('/api/user', async (req, res) => {
  try {
    // Crear una conexión a la base de datos
    const pool = await sql.connect(dbConfig);
    // Consulta SQL para obtener datos
    const result = await pool.request().query('SELECT * FROM usuario');
    // Enviar los datos obtenidos como respuesta
    res.json(result.recordset);
  } catch (error) {
    console.error('Error al obtener datos:', error);
    res.status(500).send('Error al obtener datos');
  }
});

app.get('/api/tpagos', async (req, res) => {
  try {
    // Crear una conexión a la base de datos
    const pool = await sql.connect(dbConfig);
    // Consulta SQL para obtener datos
    const result = await pool.request().query('SELECT * FROM tablapagos');
    // Enviar los datos obtenidos como respuesta
    res.json(result.recordset);
  } catch (error) {
    console.error('Error al obtener datos:', error);
    res.status(500).send('Error al obtener datos');
  }
});

app.get('/api/tpagos/:id_usuario', async (req, res) => {
  try {
    const { id_usuario } = req.params;
    const pool = await sql.connect(dbConfig);
    const result = await pool.request()
      .input('id_usuario', sql.Int, id_usuario)  // Usar el mismo nombre aquí
      .query('SELECT * FROM tablapagos WHERE id_user = @id_usuario');  // Usar el mismo nombre aquí
    res.json(result.recordset);
  } catch (error) {
    console.error('Error al obtener datos:', error);
    res.status(500).send('Error al obtener datos');
  }
});


app.post('/api/insertdata/', async (req, res) => {
  try {
    const { name, apellido, email, password } = req.body;
    const folio = "3YB9K7X2L";
    const tipo = 2;
    if (!folio || !name || !apellido || !email || !password || !tipo) {
      return res.status(400).json({ message: "Faltan campos obligatorios" });
    }
    // Crear una conexión a la base de datos
    const pool = await sql.connect(dbConfig);
    // Consulta SQL para insertar datos
    const query = `
      INSERT INTO usuario VALUES ('${folio}','${name}', '${apellido}', '${email}', '${password}','${tipo}')
    `;
    // Ejecutar la consulta
    await pool.request().query(query);
    res.status(201).json({ message: "Datos insertados correctamente" });
  } catch (error) {
    console.error('Error al insertar datos:', error);
    res.status(500).send('Error al insertar datos');
  }
});

app.post('/api/login/', async (req, res) => {
  try {
    const { email, password } = req.body;
    const pool = await sql.connect(dbConfig);
    const result = await pool.request()
      .input('email', sql.VarChar, email)
      .query('SELECT * FROM usuario WHERE email = @email');

    if (result.recordset.length === 0) {
      return res.status(404).json({ error: 'Usuario no encontrado' });
    }

    const user = result.recordset[0];

    if (user.passwordU !== password) { 
      return res.status(401).json({ error: 'Contraseña incorrecta' });
    }

    res.json({
      id_usuario: user.id_usuario,
      Nombre: user.Nombre,
      tipo: user.tipo
    });

  } catch (error) {
    console.error('Error al obtener datos del usuario:', error);
    res.status(500).send('Error al obtener datos del usuario');
  }
});

// ... (tu código existente)

app.get('/api/users/:id_usuario', async (req, res) => {
  try {
    const { id_usuario } = req.params; // Obtener ID del usuario de los parámetros de la URL

    if (!id_usuario) {
      return res.status(400).json({ message: "ID de usuario no proporcionado" });
    }

    const pool = await sql.connect(dbConfig);
    const result = await pool.request()
      .input('id_usuario', sql.Int, id_usuario)
      .query('SELECT * FROM usuario WHERE id_usuario = @id_usuario');

    if (result.recordset.length === 0) {
      return res.status(404).json({ error: 'Usuario no encontrado' });
    }

    const user = result.recordset[0];
    
    res.json({
      Nombre: user.Nombre,
      email: user.email
    });

  } catch (error) {
    console.error('Error al obtener datos del usuario:', error);
    res.status(500).send('Error al obtener datos del usuario');
  }
});

// Iniciar el servidor en el puerto especificado
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`Servidor escuchando en el puerto ${PORT}`);
});

